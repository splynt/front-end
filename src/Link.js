import React from "react";
import {Link} from "react-router-dom"
import clsx from "clsx"

export default function A(props){
    const {children,className, ...rest} = props;
    const classes = clsx({
        "btn": true,
        [className]: className
    })
    return(
        <Link {...rest} className={classes}>
            {children}
        </Link>
    )    
}
