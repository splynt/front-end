import React from "react";
import "./loader.css"

export default function Loader(){
    return(
        <div className="loader">
            <div className="loadingio-spinner-double-ring-4kqt9ac9o8"><div className="ldio-3gjueinvhob">
            <div></div>
            <div></div>
            <div><div></div></div>
            <div><div></div></div>
            </div></div>
        </div>
    )
}