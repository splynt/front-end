import React, {useState, useEffect} from 'react';
import Navbar from "./Navbar"
import useFetch from "./useFetch.js"
import Product from "./product.js"
import Loader from "./Loader.js"
import Button from "./Button.js"
import A from "./Link.js"

export default function Products() {
    const [products, setProducts] = useState([]);
    const [massdelete, setMassdelete] = useState([]);
    const {get, post, loading} = useFetch("https://devtask.konstructapp.com/api")

    useEffect(() => {
        get("/products.php")
        .then(response => {
            if(response){
                setProducts(response)
            }
        })
        .catch(error => console.log(error)) 
        // eslint-disable-next-line
    },[])


    function deleteProduct(id){
        post("/delete.php", {id: id})
        .then(response => {
            if(response){
                setProducts(response)
            }
        })
        .catch(error => console.log(error))       
    }

    function handleMassdelete(){
        if(massdelete.length === 0){
            return null
        }
        else{
            massdelete.forEach(id => {
                deleteProduct(id)
            })
        }
    }



    return (
        <>
            <Navbar title="Products">
                <A to="/addproduct" className="btn-primary">ADD</A>
                <Button className="btn-delete" onClick={handleMassdelete} disabled={loading || !massdelete.length}>MASS DELETE</Button>
            </Navbar>
            {loading && <Loader />}
            <div className="products">
                {!products.length && !loading && <p>No products yet :(</p>}
                {products && products.map((product) => {
                    return (
                        <Product
                        key={product.id}
                        details={product}
                        massdelete={massdelete}
                        setMassdelete={setMassdelete}
                        />
                    );
                })}
            </div>
        </>
    )
}