import React from 'react';

export default function Product({details,setMassdelete,massdelete}){
    function handleCheck(event){
        if(event.target.checked){
            setMassdelete([...massdelete, event.target.value])
        }
        else{
            setMassdelete(massdelete.filter(id => id !== event.target.value))
        }
    }
    return(
        <div className="product">
            <input type="checkbox" onInput={handleCheck} className="delete-checkbox" value={details.id} />   
            <div className="card">
                <p className="sku">{details.sku}</p>
                <h4 className="name">{details.name}</h4>
                <p className="price">{details.price}.00 $ </p>
                {details.productType === 'Book' && <p>Weight : {details.bookWeight} KG</p> }
                {details.productType === 'DVD' && <p>Size : {details.dvdSize} MB</p> }
                {details.productType === 'Furniture' && <p>Dimensions : {`${details.furnitureLength}x${details.furnitureHeight}x${details.furnitureWidth}`} </p> }
            </div>
        </div>
    )
}