import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Products from "./Products.js";
import NewProduct from "./NewProduct.js";

export default function App() {

    return (
        <BrowserRouter>
            <div className="container">
                <Routes>
                    <Route exact path="/" element={<Products />} />
                    <Route exact path="/addproduct" element={<NewProduct />} />
                </Routes>
                <div className="footer">
                    <p>Scandiweb Test Assignment</p>
                </div>
            </div>
        </BrowserRouter>
    );
}