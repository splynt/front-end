import React from "react";
import Input from "./Input.js"

export default function NewProductForm(props){
    const {sku, setSku, name, setName, price, setPrice, productType, size, setSize, height, setHeight, width, setWidth, length, setLength, weight, setWeight, validation, onProductChange, onFormSubmit} = props;

    return(
        <form id="product_form" onSubmit={onFormSubmit}>
            {validation &&  <div className="validation"><span>{validation}</span></div>}
            <Input 
                name="sku" 
                id="sku" 
                value={sku} 
                placeholder="Sku" 
                required  
                onChange={event => setSku(event.target.value) } 
            />
            <Input 
                name="name" 
                id="name" 
                value={name} 
                placeholder="Name" 
                required 
                onChange={event => setName(event.target.value)} 
            />
            <Input 
                type="number" 
                name="price" 
                id="price" 
                value={price} 
                placeholder="Price ($)" 
                required 
                onChange={event => setPrice(event.target.value)} 
            />
            <label className="label">
                <div>
                    Type Switcher
                </div>
                <div>
                    <select 
                        id="productType" 
                        name="productType" 
                        required 
                        value={productType} 
                        onChange={onProductChange} 
                        className="input">
                        <option value="">Select a product type</option>
                        <option value="DVD">DVD</option>
                        <option value="Book">Book</option>
                        <option value="Furniture">Furniture</option>
                    </select>
                </div>
            </label>

            {productType === "DVD" && 
                <div className="subField">
                    <h6>Please provide size of DVD</h6>
                    <Input 
                        placeholder="Size (MB)" 
                        type="number" 
                        name="size" required value={size} id="size" 
                        onChange={event => setSize(event.target.value)}
                    />
                </div>
            }
            {productType === "Furniture" &&
                <div className="subField">
                    <h6>Please provide dimensions for furniture</h6>
                    <Input
                        type="number"
                        placeholder="Height (CM)"
                        name="height" required value={height} id="height"
                        onChange={event => setHeight(event.target.value)}
                    />
                    <Input
                        type="number"
                        placeholder="Width (CM)"
                        name="width" required value={width} id="width"
                        onChange={event => setWidth(event.target.value)}
                    />
                    <Input
                        type="number"
                        placeholder="Length (CM)"
                        name="length" required value={length} id="length"
                        onChange={event => setLength(event.target.value)}
                    />
                </div>
            }

            {productType === "Book" &&
                <div className="subField">
                    <h6>Please provide weight for book</h6>
                    <Input
                        placeholder="Weight (KG)"
                        type="number"
                        name="weight" required value={weight} id="weight"
                        onChange={event => setWeight(event.target.value)}
                    />
                </div>
            }
        </form>
    )
}