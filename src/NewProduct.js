import React, {useState} from 'react';
import Navbar from "./Navbar"
import Button from "./Button.js"
import A from "./Link.js"
import ProductForm from "./ProductForm.js"
import useFetch from "./useFetch.js"
import {useNavigate} from "react-router-dom"

export default function NewProduct(){
    const [sku, setSku] = useState("");
    const [name, setName] = useState("");
    const [price, setPrice] = useState("");
    const [productType, setProductType] = useState("");
    const [size, setSize] = useState("");
    const [height, setHeight] = useState("");
    const [width, setWidth] = useState("");
    const [length, setLength] = useState("");
    const [weight, setWeight] = useState("");
    const [validation, setValidation] = useState("");
    const {post, loading} = useFetch("https://devtask.konstructapp.com/api")
    const navigate = useNavigate()

    function handleProductChange(event) {
        event.preventDefault();
        setSize("");
        setHeight("");
        setWidth("");
        setLength("");
        setWeight("");
        setProductType(event.target.value);
    }

    function handleFormSubmit(event) {
        event.preventDefault();
        if (!sku) {
            setValidation("Please enter sku first ");
            return ;
        }
        if(!name){
            setValidation("Please enter name first ");
            return;
        }
        if(!price){
            setValidation("Please enter price first ");
            return;
        }
        if(!productType){
            setValidation("Please select a product type first ");
            return;
        }
        if(!size && ( !height || !width || !length) && !weight){
            setValidation("Please enter the description of the " + productType + " first ");
            return;
        }

        setValidation("");

        post("/create.php", {
            sku: sku,
            name: name,
            price: price,
            productType: productType,
            dvdSize: size,
            furnitureHeight: height,
            furnitureWidth: width,
            furnitureLength: length,
            bookWeight: weight
        })
        .then(response => {
            console.log(response)
            if(response.success){
                console.log('success')
                setSku("");
                setName("");
                setPrice("");
                setProductType("");
                setSize("");
                setHeight("");
                setWidth("");
                setLength("");
                setWeight("");
                navigate("/")
            }
        })
        .catch(error => {console.log(error)})

    }

    const formActions = {sku, setSku, name, setName, price, setPrice, productType, size, setSize, height, setHeight, width, setWidth, length, setLength, weight, setWeight, validation, setValidation};


    return (
        <>
            <Navbar title="New Product">
                <Button className="btn-primary" onClick={handleFormSubmit} disabled={loading} >Save</Button>
                <A className="btn-delete" to="/">Cancel</A>
            </Navbar>
            <ProductForm {...formActions} onProductChange={handleProductChange} onFormSubmit={handleFormSubmit} />
        </>
    );
}