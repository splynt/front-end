import React from "react";

export default function Navbar(props){
    return(
        <nav className="navbar">
            <div className="navbar_logo">
                <h1>{props.title}</h1>
            </div>
            <div className="navbar__buttons">
                {props.children}
            </div>
        </nav>
    )
}
